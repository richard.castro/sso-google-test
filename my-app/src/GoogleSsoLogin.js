import React, { useEffect, useState } from 'react'
import { GoogleOAuthProvider, GoogleLogin } from '@react-oauth/google'
import jwt_decode from "jwt-decode"


const Google = () => {
    // Boton de login sso
    const spInitSSO = (email) => {
        const portalId = 13
        const campusId = 484
        const url = process.env.NODE_ENV === 'production'
            ? `https://spcv.eclass.com/sso/spinitsso-redirect-google?id=google&portal=${portalId}&campus=${campusId}&email=${email}`
            : `http://localhost:4004/sso/spinitsso-redirect-google?id=google&portal=${portalId}&campus=${campusId}&email=${email}`
        window.location.replace(url)
    }

    const [credentials, setCredentials] = useState("");

    useEffect(() => {
        console.log('entro aqui', credentials)
        if (credentials !== "") {
            console.log('hay credenciales')
            spInitSSO(credentials.email)
        }
    }, [credentials])

    return (
        <section className="google">
            <div className="form-group">
                <div className="text-center">
                    <GoogleOAuthProvider
                        clientId="927626894491-g1ug64p12mck191hj1sqtpujdevnh295.apps.googleusercontent.com"
                    >
                        <GoogleLogin
                            onSuccess={credentialResponse => {
                                console.log(credentialResponse)
                                let credentials = jwt_decode(credentialResponse.credential)
                                setCredentials(credentials);
                                // console.log(credentials)
                            }}
                            onError={() => {
                                console.log('Login Failed')
                            }}
                        />
                    </GoogleOAuthProvider>
                </div>
            </div>
        </section>
    )
}

export default Google
